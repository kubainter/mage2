<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'JGrzesiak_CountdownToDelivery',
    __DIR__
);
