<?php

namespace JGrzesiak\CountdownToDelivery\Block;

use Magento\Framework\App\Config\ScopeConfigInterface;
use DateTime;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;

class CountdownToDelivery extends Template
{
    protected ScopeConfigInterface $scopeConfig;

    public function __construct(Context $context, ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;

        parent::__construct($context);
    }

    public function getDeliveryDate(): DateTime
    {
        $currentdate = (new DateTime());

        if ($this->isInExcludeDays($currentdate)) {
            $currentdate->modify('+1Day');
        }

        return $currentdate;
    }

    public function getCutOffTime(): int
    {
        $dateNow      = new DateTime();
        $cuttOffValue = $this->scopeConfig->getValue(
            'countdowntodelivery/delivery/cutofftime' . mb_strtolower($dateNow->format('D')),
            ScopeInterface::SCOPE_STORE
        );

        if (empty($cuttOffValue)) {
            return 0;
        }

        $hoursSplitted = explode(":", $cuttOffValue);
        $cutoffTime    = strtotime($dateNow->setTime($hoursSplitted[0], $hoursSplitted[1], 0)->format('H:i:s'));
        $currentTime   = $this->getCurrentTime();

        if ($currentTime > $cutoffTime) {
            return 0;
        }

        return $cutoffTime;
    }

    public function getTimeRemaining(): string
    {
        if (is_null($this->getCutOffTime())) {
            return 0;
        }

        return date('H:i:s', mktime(0, 0, $this->getCutOffTime() - $this->getCurrentTime()));
    }

    public function getCurrentTime(): int
    {
        return strtotime((new DateTime())->format('H:i:s'));
    }

    public function getTimeRemainingSeconds()
    {
        return $this->getCutOffTime() > 0 ? $this->getCutOffTime() - $this->getCurrentTime() : 0;
    }

    public function buildString(): string
    {
        $string = $this->scopeConfig->getValue(
            'countdowntodelivery/general/string', ScopeInterface::SCOPE_STORE
        );

        if ($this->getDeliveryDate()->format('d-m-Y') != (new DateTime())->format('d-m-Y')) {
            return $this->scopeConfig->getValue(
                'countdowntodelivery/general/stringoff', ScopeInterface::SCOPE_STORE
            );
        }

        return str_replace("{{time_remaining}}", '<span id="time">' . $this->getTimeRemaining() . "</span>", $string);
    }

    private function isInExcludeDays($today): bool
    {
        $excludedays = $this->scopeConfig->getValue(
            'countdowntodelivery/delivery/excludedays', ScopeInterface::SCOPE_STORE
        );

        $excludedays = json_decode($excludedays);

        if (empty($excludedays)) {
            return false;
        }

        foreach ($excludedays as $day) {
            if (empty($day->field)) {
                continue;
            }
            
            if ($this->checkIfExcluded($today, $day->field2)) {
                return true;
            }
        }

        return false;
    }

    private function checkIfExcluded($today, $excludedDay): bool
    {
        if (!$today instanceof DateTime) {
            $today = new DateTime($today);
        }

        if (!$excludedDay instanceof DateTime) {
            $excludedDay = DateTime::createFromFormat('d.m.Y', $excludedDay);
        }

        $interval = $today->diff($excludedDay);

        return $interval->days == 0;
    }
}
