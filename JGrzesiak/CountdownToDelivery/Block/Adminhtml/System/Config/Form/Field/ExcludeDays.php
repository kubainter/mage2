<?php

namespace JGrzesiak\CountdownToDelivery\Block\Adminhtml\System\Config\Form\Field;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\Data\Form\Element\Factory;

class ExcludeDays extends AbstractFieldArray
{
    /**
     * @var Factory
     */
    protected $_elementFactory;

    /**
     * @param Context $context
     * @param Factory $elementFactory
     * @param array   $data
     */
    public function __construct(Context $context, Factory $elementFactory, array $data = [])
    {
        $this->_elementFactory = $elementFactory;

        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->addColumn('field1', ['label' => __('Name')]);
        $this->addColumn('field2', ['label' => __('Date (format: m.d.Y)')]);

        $this->_addAfter       = false;
        $this->_addButtonLabel = __('Add');

        parent::_construct();
    }

}
